import './App.css';
import { Route, Link } from "react-router-dom";
import { Header } from "./shared/component/header.component";
import { PokemonListPage } from "./pages/pokemon_list/pokemon-list.page";
import { PokemonDetailPage } from "./pages/pokemon_detail/pokemon-detail.page";
import { MyPokemonListPage } from "./pages/my_pokemon_list/my-pokemon-list.page";
import styled from "@emotion/styled";
import { useState } from 'react';

const AppWrapper = styled.div `
  width: 100%;
  height: 100vh;
  background-color: #edfcff;
`;

const NavBar = styled.ul `
  width: 100%;
  height: 30px;
  background-color: #64b4f9;
  display: flex;
  margin: 0px;
  padding: 0px;
`;

const NavBarItem = styled.li `
background-color: #bbdfff;
  width: 150px;
  list-style-type: none;
  border-radius: 14px 14px 0px 0px;
  padding-top: 4px;

  &.active {
    background-color: #edfcff;
    color: black;
  }

  a {
    text-decoration: none;
    margin-top: 8px;
  }
`;

export function App() {
  const pathName = (window.location.pathname === '/') ? 'list' : 'my-list';
  const [activeRoute, setActiveRoute] = useState(pathName);

  return <AppWrapper className="App">
    <Header></Header>
    <NavBar>
      <NavBarItem className={(activeRoute === 'list') ? 'active' : ''}>
        <Link onClick={() => setActiveRoute('list')} to="/">Pokemon List</Link>
      </NavBarItem>
      <NavBarItem className={(activeRoute === 'my-list') ? 'active' : ''}>
        <Link onClick={() => setActiveRoute('my-list')} to="/my-pokemon-list">My Pokemon</Link>
      </NavBarItem>
    </NavBar>

    <Route exact path="/" component={ PokemonListPage }/>
    <Route exact path="/pokemon-detail/:name" component={ PokemonDetailPage }/>
    <Route exact path="/my-pokemon-list" component={ MyPokemonListPage } />
  </AppWrapper>
}
