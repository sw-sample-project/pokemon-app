import pokemonLogo from "../../assets/imgs/pokemon-logo.png";
import styled from "@emotion/styled";
import { mobile } from "../media-query";

const HeaderWrapper = styled.div `
  width: 100%;
  height: 170px;
  background-color: #64b4f9;
`;

const PokemonLogo = styled.img `
  width: 352px;
  margin-top: 21px;
  @media (${mobile}) {
    width: 272px;
    margin-top: 37px;
  }
`;

export const Header = () => {
  return <HeaderWrapper id="pokemon-app-header">
    <PokemonLogo src={ pokemonLogo }></PokemonLogo>
  </HeaderWrapper>  
}