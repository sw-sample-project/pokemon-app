import { ApolloClient, InMemoryCache } from '@apollo/client';
import { gql } from '@apollo/client';

let graphql;
export let count = 0;
export function initialGraphQl() {
  graphql = new ApolloClient({
    uri: 'https://graphql-pokeapi.vercel.app/api/graphql',
    cache: new InMemoryCache()
  });
}

export function getPokemonList(limit, offset) {
  return graphql.query({
    query: gql `
      query {
        pokemons(limit:${limit}, offset:${offset}) {
          count
          next
          previous
          status
          message
          results {
            url
            name
            image
            id
          }
        }
      }
    `
  });
}

export function getPokemonDetailByName(pokemonName) {
  return graphql.query({
    query: gql `
      query {
        pokemon(name: "${pokemonName}") {
          id
          name
          moves {
            move {
              name
            }
          }
          types {
            type {
              name
            }
          }
          message
          status
        }
      }
    `
  });
}