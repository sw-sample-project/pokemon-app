function PokemonItem(props) {
  const pokemon = props.pokemon;
  const onRelease = props.onRelease;
  const index = props.index;

  return <div className="pokemon-item">
    <img alt="pokemon-logo" className="pokemon-img" src={ pokemon.image }/>
    <span className="pokemon-name">{ pokemon.nickname }</span>
    <button className="release-btn" onClick={() => onRelease(pokemon, index) }>Release</button>
  </div>
}

export function MyPokemonListView(props) {
  const myPokemonList = props.myPokemonList;
  const onRelease = props.onRelease;

  return <div className="my-pokemon-list-page">
    {
      myPokemonList.map((pokemon, index) =>
        <PokemonItem key={pokemon.id} pokemon={pokemon} onRelease={onRelease} index={index}></PokemonItem>
      )
    }
  </div>
}