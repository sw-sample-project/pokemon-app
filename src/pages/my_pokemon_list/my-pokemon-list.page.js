import { useEffect, useState } from 'react';
import './my-pokemon-list.page.scss';
import { MyPokemonListView } from "./my-pokemon-list.view";

let _myPokemonList = undefined;

function getMyPokemonList() {
  let result = [];
  for(const property in _myPokemonList) {
    const pokemon = _myPokemonList[property];

    pokemon.nickname.forEach(_nickname => {
      let temp = JSON.parse(JSON.stringify(pokemon));
      temp.nickname = _nickname;
      temp.id = result.length;
      delete temp.numberOfOwnership;
      result.push(temp);
    });
  }
  return result;
}

export function MyPokemonListPage() {
  const [myPokemonList, setMyPokemonList] = useState([]);

  useEffect(() => {
    if (myPokemonList.length === 0) {
      _myPokemonList = (localStorage.getItem('my-pokemon-list')) ? JSON.parse(localStorage.getItem('my-pokemon-list')) : undefined;
      const myPokemonListFromLocale = getMyPokemonList();
      setMyPokemonList([...myPokemonList, ...myPokemonListFromLocale]);
    }
  }, []);

  const onRelease = (pokemon, index) => {
    const temp = JSON.parse(JSON.stringify(myPokemonList));
    temp.splice(index, 1);
    setMyPokemonList(temp);

    _myPokemonList[pokemon.name].nickname.splice(index, 1);
    _myPokemonList[pokemon.name].numberOfOwnership -= 1;
    localStorage.setItem('my-pokemon-list', JSON.stringify(_myPokemonList));
  }

  return <MyPokemonListView myPokemonList={ myPokemonList } onRelease={ onRelease }></MyPokemonListView>
}