import './pokemon-detail.page.scss';
import { useEffect, useState } from "react";
import { getPokemonDetailByName } from "../../shared/services/graphql.service";
import { PokemonDetailView } from "./pokemon-detail.view";

export function PokemonDetailPage(props) {
  const pokemmon = props.location.state.pokemon;
  const routerParamsName = props.match.params.name;
  const [moves, setMoves] = useState(undefined);
  const [types, setTypes] = useState(undefined);
  let getMyPokemonList = undefined;

  useEffect(() => {
    if (routerParamsName && !moves && !types) {
      getPokemonDetailByName(pokemmon.name).then(_res => {
        const detailPokemon = _res.data.pokemon;
        let moves = [];
        let types = [];

        detailPokemon.moves.forEach(_element => { moves.push(_element.move.name); });
        detailPokemon.types.forEach(_element => { types.push(_element.type.name); });

        setMoves(moves.join(', '));
        setTypes(types.join(', '));
      });
    }
  });

  function handleCatch() {
    let ramdom = Math.floor(Math.random() * Math.floor(2));
    switch(ramdom) {
      case 1: catchSuccessFull(); break;
      case 0: alert('Catch failed'); break;
      default: break;
    };
  }

  function catchSuccessFull() {
    let nickname = prompt(`${pokemmon.name} was arrested, gave a nickname`, 'Nickname');
    if (nickname === ' ' || nickname === 'Nickname' || !nickname) {
      if (nickname === ' ' || nickname === 'Nickname') {
        alert('Nickname must be filled');
        handleCatch();
      }
    } else {
      getMyPokemonList = (localStorage.getItem('my-pokemon-list')) ? JSON.parse(localStorage.getItem('my-pokemon-list')) : undefined;
      switch(getMyPokemonList === undefined) {
        case true: handleEmptyData(nickname); break;
        case false: addNewPokemon(getMyPokemonList[pokemmon.name] !== undefined, nickname); break;
        default: break;
      }
    }
  }

  function handleEmptyData(nickname) {
    let tempPokemon = {
      [pokemmon.name]: JSON.parse(JSON.stringify(pokemmon))
    };
    tempPokemon[pokemmon.name]['nickname'] = [nickname];
    tempPokemon[pokemmon.name]['numberOfOwnership'] = 1;
    localStorage.setItem('my-pokemon-list', JSON.stringify(tempPokemon));
  }

  function addNewPokemon(pokemonIsExsist, nickname) {
    switch(pokemonIsExsist) {
      case false: addFreshPokemon(nickname); break;
      case true: addToExistingPokemon(nickname); break;
      default: break;
    }
  }

  function addFreshPokemon(nickname) {
    let tempPokemon = JSON.parse(JSON.stringify(pokemmon));
    tempPokemon.nickname = [nickname];
    tempPokemon.numberOfOwnership = 1;
    getMyPokemonList[pokemmon.name] = tempPokemon;
    localStorage.setItem('my-pokemon-list', JSON.stringify(getMyPokemonList));
  }

  function addToExistingPokemon(nickname) {
    getMyPokemonList[pokemmon.name].nickname.push(nickname);
    getMyPokemonList[pokemmon.name].numberOfOwnership += 1;
    localStorage.setItem('my-pokemon-list', JSON.stringify(getMyPokemonList));
  }

  return <PokemonDetailView 
    pokemon={pokemmon} 
    routerParamsName={routerParamsName} 
    moves={moves} types={types} 
    handleCatch={handleCatch}>
  </PokemonDetailView>
}