import pokeball from "../../assets/imgs/pokeball.png";

export function PokemonDetailView(props) {
  const pokemon = props.pokemon;
  const routerParamsName = props.routerParamsName;
  const moves = props.moves;
  const types = props.types;
  const handleCatch = props.handleCatch;

  return <div className="pokemon-detail-wrapper"> 
  <div className="detail-content">
    <img alt="pokemon" className="pokemon-img" src={ pokemon.image }/>

    <div className="pokemon-info-wrapper">
      <span className="pokemon-name">{ routerParamsName }</span>
      <label className="label-field">Pokemon moves:</label>
      <div className="text-wrapper">
        <p className="pokemon-moves">{ moves }</p>
      </div>

      <label className="label-field">Pokemon types:</label>
      <div className="text-wrapper">
        <p className="pokemon-types">{ types }</p>
      </div>

      <button className="catch-button" onClick={ handleCatch }>
        <span>CATCH </span><img alt="pokeball" src={ pokeball } />
      </button>
    </div>
  </div>
</div>
}