import './pokemon-list.page.scss';
import { getPokemonList } from "../../shared/services/graphql.service";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

let getMyPokemonList = undefined;

function PokemonItem(props) {
  const pokemon = props.pokemon;
  const history = useHistory();
  const handleClick = (pokemon) => {
    history.push({
      pathname: `pokemon-detail/${pokemon.name}`,
      state: { pokemon }
    });
  }

  function NumberOfOwnership() {
    let numberOfOwnership = 0;
    if (getMyPokemonList && getMyPokemonList[pokemon.name]) {
      numberOfOwnership = getMyPokemonList[pokemon.name].numberOfOwnership;
    }
    return <span className="text">{ numberOfOwnership }</span>
  }

  return <div className="pokemon-item" onClick={() => handleClick(pokemon)}>
    <img alt="pokemon-logo" className="pokemon-img" src={ pokemon.image }/>
    <span className="pokemon-name">{ pokemon.name }</span>
    <div className="number-have-this-pokemon">
      <NumberOfOwnership></NumberOfOwnership>
    </div>
  </div>
}

export function PokemonListPage() {
  const [pokemonList, setPokemonList] = useState([]);
  const [offset, setOffset] = useState(0);
  const [isFetch, setIsFetch] = useState(false);
  const [limit, setLimit] = useState(15);

  function fetchList(procedure, limit, offset) {
    getPokemonList(limit, offset).then(_res => {
      const result = _res.data.pokemons.results;
      setPokemonList([...pokemonList, ...result]);
      setOffset(offset + 15);

      if (procedure === 'on_scroll') {
        const totalData = _res.data.pokemons.count;
        if (totalData - pokemonList.length < 5) { setLimit(totalData - pokemonList.length); }
        setIsFetch(false);
      }
    });
  }

  useEffect(() => {
    if (pokemonList.length === 0) {
      getMyPokemonList = (localStorage.getItem('my-pokemon-list')) ? JSON.parse(localStorage.getItem('my-pokemon-list')) : undefined;
      fetchList('inital_pages', limit, offset);
    }
  }, []);

  const onScroll = e => {
    if (e.target.scrollTop === e.target.scrollTopMax && !isFetch) {
      setIsFetch(true);
      fetchList('on_scroll', limit, offset);
    }
  };

  return <div className="pokemon-list-page" onScroll={onScroll}>
    { 
      pokemonList.map((pokemon) => 
        <PokemonItem key={pokemon.name} pokemon={pokemon}></PokemonItem>
      )
    }
  </div>
}